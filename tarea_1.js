//Luis Raúl Chacón Muñoz - 339011

//1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json
//Comandos:
docker start mongo-22
docker cp grades.json mongo-22:/tmp/grades.json
docker exec -ti mongo-22 /bin/bash

//Salidas:
/*mongo-22

mongoimport -d students -c grades < /tmp/grades.json
2022-11-09T22:42:00.273+0000    connected to: mongodb://localhost/
2022-11-09T22:42:00.369+0000    800 document(s) imported successfully. 0 document(s) failed to import.
*/

//2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?
//Comandos:
mongosh
use students
db.grades.count()

//Salidas
/*
switched to db students
DeprecationWarning: Collection.count() is deprecated. Use countDocuments or estimatedDocumentCount.
800*/

//3) Encuentra todas las calificaciones del estudiante con el id numero 4.
//Comando
db.grades.find({"student_id":4})
//Salida
/*
[
    {
      _id: ObjectId("50906d7fa3c412bb040eb588"),
      student_id: 4,
      type: 'quiz',
      score: 27.29006335059361
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb587"),
      student_id: 4,
      type: 'exam',
      score: 87.89071881934647
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb589"),
      student_id: 4,
      type: 'homework',
      score: 5.244452510818443
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb58a"),
      student_id: 4,
      type: 'homework',
      score: 28.656451042441
    }
  ]*/

//Los scores son: 27.29006335059361, 87.89071881934647,
//                5.244452510818443, 28.656451042441

//4) ¿Cuántos registros hay de tipo exam?
//Comando
db.grades.find({"type":"exam"}).count()
//Salida
//200

//5) ¿Cuántos registros hay de tipo homework?
//Comando
db.grades.find({"type":"homework"}).count()
//Salida
//400

//6) ¿Cuántos registros hay de tipo quiz?
//Comando
db.grades.find({"type":"quiz"}).count()
//Salida
//200

//7) Elimina todas las calificaciones del estudiante con el id numero 3
//Comando
db.grades.remove({student_id:3})

//Salida
/*
DeprecationWarning: Collection.remove() is deprecated. Use deleteOne, deleteMany, findOneAndDelete, or bulkWrite.
{ acknowledged: true, deletedCount: 4 }
*/

//8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
//Comando
db.grades.find({"score":75.29561445722392,"type":"homework"})

//Salida
/*[
    {
      _id: ObjectId("50906d7fa3c412bb040eb59e"),
      student_id: 9,
      type: 'homework',
      score: 75.29561445722392
    }
  ]*/


//9) Actualiza las calificaciones del registro con el uuid Ob por 100
//Comando
db.grades.updateOne({"_id":ObjectId("50906d7fa3c412bb040eb591")}, {$set:{"score":100}})

//Salida
/*
{
    acknowledged: true,
    insertedId: null,
    matchedCount: 1,
    modifiedCount: 1,
    upsertedCount: 0
  }
*/

//10) A qué estudiante pertenece esta calificación*/
//Comando
db.grades.find({"_id":ObjectId("50906d7fa3c412bb040eb591")})

//Salida
/*
[
    {
      _id: ObjectId("50906d7fa3c412bb040eb591"),
      student_id: 6,
      type: 'homework',
      score: 100
    }
  ]
  
  */